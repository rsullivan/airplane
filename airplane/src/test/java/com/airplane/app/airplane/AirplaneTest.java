package com.airplane.app.airplane;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.mockito.Mockito.*;

public class AirplaneTest {

	

	

	


	
	@Test
	public void hasAnEngine(){
		
		Engine engine = new Engine();
		Airplane classUnderTest = new Airplane(engine);
		String expectedAnswer = "This is an Airplane";
		String actualAnswer = classUnderTest.toString();
		assertEquals(expectedAnswer, actualAnswer);
		
	}
	
	@Test
	public void airplaneDependentOnEngine(){
		
		Engine engine = new Engine();
		Airplane classUnderTest = new Airplane(engine);
		String expectedAnswer = engine.toString();
		String actualAnswer = classUnderTest.getEngineRef();
		assertEquals(expectedAnswer, actualAnswer);
		
	}
	
	@Test
	public void mockedAirplaneTest(){
		String actualAnswer, mockedAnswer;
		Engine engine = new Engine();
		Airplane airplane =  new Airplane(engine);
		
		
		Airplane mockedAirplane = mock(Airplane.class);
		when(mockedAirplane.toString()).thenReturn("This is an Airplane");
		
		actualAnswer = airplane.toString();
		mockedAnswer = mockedAirplane.toString();
		
		assertEquals("Wrong answer",actualAnswer, mockedAnswer);
	}
	
	@Test
	public void jetClassDependentOnAirplane(){
		Engine jet = new Engine();
		Jet classUnderTest = new Jet(jet);
		String expectedAnswer = jet.toString();
		String actualAnswer = classUnderTest.getEngineRef();
		assertEquals(expectedAnswer, actualAnswer);
		
		
	}
	
	

}
